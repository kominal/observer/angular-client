import { ComponentType } from '@angular/cdk/portal';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { MqttService } from 'ngx-mqtt';

export const TENANT_ID = new InjectionToken<string | undefined>('OBSERVER_TENANT_ID');
export const PROJECT_NAME = new InjectionToken<string | undefined>('OBSERVER_PROJECT_NAME');
export const ENVIRONMENT_NAME = new InjectionToken<string | undefined>('OBSERVER_ENVIRONMENT_NAME');
export const SERVICE_NAME = new InjectionToken<string | undefined>('OBSERVER_SERVICE_NAME');
export const SNACK_BAR_COMPONENT = new InjectionToken<string | undefined>('OBSERVER_SNACK_BAR_COMPONENT');

export enum Level {
	DEBUG = 'DEBUG',
	INFO = 'INFO',
	WARN = 'WARN',
	ERROR = 'ERROR',
}

@Injectable({
	providedIn: 'root',
})
export class ObserverClientService {
	taskId = this.generateToken(24);
	remoteLoggingEnabled = false;

	constructor(
		@Inject(TENANT_ID) private tenantId: string,
		@Inject(PROJECT_NAME) private projectName: string,
		@Inject(ENVIRONMENT_NAME) private environmentName: string,
		@Inject(SERVICE_NAME) private serviceName: string,
		@Inject(SNACK_BAR_COMPONENT) private snackBarComponent: ComponentType<any>,
		private translateService: TranslateService,
		private mqttService: MqttService,
		private snackBar: MatSnackBar
	) {
		this.remoteLoggingEnabled = !!tenantId && !!projectName && !!environmentName && !!serviceName;
	}

	private generateToken(length: number) {
		var result = '';
		var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	private print(level: Level, message: any) {
		try {
			let color = '#ffff00';
			if (level === Level.DEBUG) {
				color = '#00b0ff';
			} else if (level === Level.INFO) {
				color = '#bada55';
			} else if (level === Level.WARN) {
				color = '#ffd700';
			} else if (level === Level.ERROR) {
				color = '#f44336';
			}

			if (typeof message === 'string') {
				if (message.startsWith('transl.')) {
					message = this.translateService.instant(message);
				}
			} else {
				message = JSON.stringify(message);
			}

			const consoleMessage = `${new Date().toLocaleTimeString(undefined, { hour12: false })} | [%c${level}%c] ${
				typeof message === 'string' ? message : JSON.stringify(message)
			}`;

			if (level === Level.DEBUG) {
				console.debug(consoleMessage, `color: ${color}`, '');
			} else if (level === Level.INFO) {
				console.info(consoleMessage, `color: ${color}`, '');
			} else if (level === Level.WARN) {
				console.warn(consoleMessage, `color: ${color}`, '');
			} else if (level === Level.ERROR) {
				console.error(consoleMessage, `color: ${color}`, '');
			} else {
				console.log(consoleMessage, `color: ${color}`, '');
			}

			if (this.snackBarComponent) {
				this.snackBar.openFromComponent(this.snackBarComponent, { data: { level, message }, duration: 3000 });
			} else {
				this.snackBar.open(message, '', { duration: 3000 });
			}
		} catch (e) {}
	}

	public sendEvent(type: string, content: any): void {
		if (!this.remoteLoggingEnabled) {
			return;
		}
		try {
			this.mqttService.unsafePublish(
				`${this.tenantId}/${this.projectName}/${this.environmentName}/${this.serviceName}/${this.taskId}/event`,
				JSON.stringify({
					time: new Date(),
					type,
					content,
				}),
				{ qos: 0 }
			);
		} catch (e) {
			this.log(Level.ERROR, `Could not forward event to Observer: ${e}`);
		}
	}

	async log(level: Level, message: any) {
		this.print(level, message);
		if (!this.remoteLoggingEnabled) {
			return;
		}
		try {
			this.mqttService.unsafePublish(
				`${this.tenantId}/${this.projectName}/${this.environmentName}/${this.serviceName}/log`,
				JSON.stringify({
					time: new Date(),
					level,
					message,
				}),
				{ qos: 0 }
			);
		} catch (e) {
			this.print(Level.ERROR, `Could not forward log message to Observer: ${e}`);
		}
	}

	async debug(message: any) {
		return this.log(Level.DEBUG, message);
	}

	async info(message: any) {
		return this.log(Level.INFO, message);
	}

	async warn(message: any) {
		return this.log(Level.WARN, message);
	}

	async error(message: any) {
		return this.log(Level.ERROR, message);
	}

	async handleError(e: any) {
		if (typeof e === 'string') {
			return this.error(e);
		} else if (e.error?.error) {
			return this.error(e.error.error);
		} else if (e.message) {
			this.error(e.message);
			console.trace(e);
		} else {
			this.error('transl.error.unknown');
			console.trace(e);
		}
	}
}
