import { ComponentType } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MqttModule } from 'ngx-mqtt';
import { ENVIRONMENT_NAME, PROJECT_NAME, SERVICE_NAME, SNACK_BAR_COMPONENT, TENANT_ID } from './services/observer-client.service';

export function registerObserverMqttModule(): ModuleWithProviders<MqttModule> {
	return MqttModule.forRoot({
		protocol: 'wss',
		hostname: 'mqtt.observer.kominal.app',
		port: 8084,
		path: '/mqtt',
	});
}

@NgModule({
	declarations: [],
	imports: [CommonModule],
})
export class ObserverClientModule {
	static forRoot(config: {
		tenantId: string;
		projectName: string;
		environmentName: string;
		serviceName: string;
		snackBarComponent?: ComponentType<any>;
	}): ModuleWithProviders<ObserverClientModule> {
		return {
			ngModule: ObserverClientModule,
			providers: [
				{ provide: TENANT_ID, useValue: config.tenantId },
				{ provide: PROJECT_NAME, useValue: config.projectName },
				{ provide: ENVIRONMENT_NAME, useValue: config.environmentName },
				{ provide: SERVICE_NAME, useValue: config.serviceName },
				{ provide: SNACK_BAR_COMPONENT, useValue: config.snackBarComponent },
			],
		};
	}
}
